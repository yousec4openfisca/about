# Yousec 4 Open Fisca
> Yet Open Undecided Software Engineering Cocktail For Openfisca

[![Yousec](https://gitlab.com/yousec4openfisca/about/raw/master/img/yousec-couverture.png)](https://twitter.com/YousecOpenfisca)

## Contexte
Ce projet a été réalisé dans le cadre du Hackathon [#codeinpot][] réalisé par l'[etalab][] et la [DGFip][] le 1er et 2 Avril.
La vocation est d'accompagner le lancement d'[OpenFisca][], constitant à l'ouverture du calculateur des impots


Pour plus de détails, se référer à la [présentation par l'Etalab][hack-pres]

## Proposition de Valeur
### Vision

Notre vision est de mettre au point des __outils et pratiques pour faciliter la création, maintenance du développeent d'extension _open fisca_ tout en relevant la qualité logicielle__.
Tout ceci en appliquant un __métissage__ des _pratiques_ et _credos_ [_craftmans_](http://manifesto.softwarecraftsmanship.org/), d'ingénierie logicielle modernes [__agiles__](http://agilemanifesto.org/iso/fr/), [__intégration continue__](https://fr.wikipedia.org/wiki/Intégration_continue) et [__devops__](http://theagileadmin.com/2010/10/15/a-devops-manifesto/).


### Idées Envisigées

Pour réaliser mettre en oeuvre cette vision, plusieurs axes complémentaires sont envisagés.

Plusieurs graines idées à explorer pour rendre [M][]&consort plus facilement accessible au développeur, métier et plus si affinité.

Parmi les quels (ensemble non ordonné):

- une __[API][]__(_Interface_) [facade][] données semi structurées
- un __[DSL][]__(_langage dédié_) python avec une meilleur [DX]()(_eXperience Développer_/_Metier_)
- Specification texte facile à écrire et comprendre par le métier.
- Un Generateur de skelette pour les extension open fiscal, pour mettre en place outillage et bonnes pratiques dès le départ


### Status Actuel



### Réalisation concretes

*Patience, ça avance :wink:*


## Pour nous suivre
### Liens et Communication
- __twitter__ : https://twitter.com/YousecOpenfisca
- __Dépot git__:
  + _groupe_ : https://gitlab.com/yousec4openfisca
  + _présent README_ : https://gitlab.com/yousec4openfisca/about
- __trello__:
  + _organisation_ : https://trello.com/yousec4openfisca
- __mail__ : mailto:yousec4openfisca@gmail.com

### Qui nous sommes

- Adrien Becchis (_detaux en attente_)
- Alexandre Milisavljevic (_detaux en attente_)

<!-- ### Ressources recommandées
définitions
articles

 ## Quelques credo suivis :: craftmanship -->


<!-- Links-->
[OpenFisca]: http://www.openfisca.fr/
[#codeinpot]: https://twitter.com/hashtag/codeimpot?f=tweets&vertical=default&src=hash
[etalab]: https://www.etalab.gouv.fr/
[DGFiP]: http://www.economie.gouv.fr/dgfip
[hack-pres]: https://www.etalab.gouv.fr/codeimpot-un-hackathon-autour-de-louverture-du-code-source-du-calculateur-impots
[DSL]: https://fr.wikipedia.org/wiki/Langage_d%C3%A9di%C3%A9
[API]: https://fr.wikipedia.org/wiki/Interface_de_programmation
[facade]: https://fr.wikipedia.org/wiki/Fa%C3%A7ade_(patron_de_conception)
